# README #

To jest przykładowy plik README w repozytorium stworzonym na potrzeby kolokwium.

## Zadanie ##

1. Należy wykonać fork na własne konto i pracować na owym forku. W przypadku braku możliwości wykonania forka, proszę kontynuować - zmiana w sposobie oddania kolokwium do sprawdzenia zostanie opisana poniżej. Do tego czasu wszelkie odniesienia do "swojego forka" proszę traktować jako odniesienia do tego repozytorium.
2. Proszę sklonować swojego forka na dysk lokalny (link po lewej, pojawi się też link do pomocy). Dalsze polecenia odnoszą się do pracy na kopii repozytorium na dysku lokalnym. Proszę czytać komunikaty, gdyż może się okazać, że do dalszej pracy konieczne jest wstępne skonfigurowanie GITa, np. podanie nazwiska lub emaila.
3. Proszę stworzyć gałąź o nazwie **dev** i przełączyć się na nią.
4. Proszę zmodyfikować plik README.md, wykasować trzecią linijkę ("To jest przykładowy...") i w jej miejscu umieścić swój numer legitymacji. Po zapisaniu pliku proszę go zapamiętać oraz wykonać commit z komunikatem "numer indeksu".
5. Proszę stworzyć plik hello.cpp, napisać wewnątrz program typu "hello world", dodać do śledzenia i zacommitować z komunikatem "hello world"
6. Proszę z konsoli git-bash wywołać polecenie `history >historia.txt` a następnie dodać powstały plik do śledzenia i zacommitować z komunikatem "historia pracy"
7. Proszę przesłać gałąź **dev** do swojego forka w serwisie bitbucket (osoby bez forka proszone są o dopisanie na końcu pliku `historia.txt` polecenia, które wykonałoby to zadanie).
8. (Osoby bez forka pomijają to polecenie) Jeszcze raz proszę wywołać z konsoli git-bash polecenie `history >historia.txt`
9. (Osoby bez forka pomijają to polecenie) Proszę dodać wszystkie zmodyfikowane pliki do repozytorium, zacommitować z komentarzem "zakończenie" i wysłać na serwer (do swojego forka).
10. (Osoby bez forka pomijają to polecenie) Będąc na swoim forku proszę wykonć PullRequest do oryginalnego repozytorium ze swojej gałęzi **dev**. W opisie pull-requesta najlepiej wpisać swój numer legitymacji.
11. (Tylko dla osób bez własnego forka) Osoby bez forka proszone są o przesłanie pliku historia.txt na adres ath@kozaczko.info z tematem 'Kolokwium GIT - {numer legitymacji}'